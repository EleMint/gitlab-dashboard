# GitLab Dashboard

## Settings

### Configuration

- GitLab Instance
  - Defaults to `https://gitlab.com`
  - Set with `env: GITLAB_INSTANCE` or input
- GitLab Personal Access Token
  - If empty, user is redirected to the `Settings` page
  - Set with `env: GITLAB_PAT` or input
  - Requires the `read_api` PAT scope
- Poll Interval
  - Value in seconds
  - Defaults to `60 (1 min)`
  - Determines time between data refresh
  - Set to `0` to disable polling

### Groups & Projects

Select Groups and Projects/Repositories you wish to view in the `Merge Requests` page. This selection also determines which Groups and Projects/Repositories are available in the sidebar

![Settings Page](imgs/settings.png)

## Merge Requests

- Click on `Merge Requests` to view all MRs included in Groups and Projects/Repositories (configure in Settings)
- Click on a Group or Project/Repository to view all MRs in the selected Group or Project/Repository
- Filter MRs by
  - state: Radio (`Closed` | `Locked` | `Merged` | `Opened`)
  - draft: Toggle (`Draft: <MR>`)
- MR Actions
  - Click `Group Name` will open the Group in the browser
  - Click `Group/Project ID` will copy the ID to clipboard
  - Click `MR Title` will open the MR in the browser
  - Click `MR Author` will open the GitLab user in the browser
  - Hover `Created/Updated time distance` to view actual date-time value

![Group MRs](imgs/group.png)
![Project MRs](imgs/project.png)

## Sidebar

Use `ctrl-s` or `Double-Click` on the border to hide/show sidebar

![Collapsed Sidebar](imgs/collapse-sidebar.png)

## Future Iterations

- `CI/CD` release pipeline
- `Auto-Update` when a new release is published
- Tab or Button to view Group or Project/Repository `Pipelines`
- Display MR `Pipeline` status
- Display MR `Approval` status
  - Current number of approvers
  - Number of total required approvers
- Ability to `Search` for MRs
- Show available `Hotkeys` in Settings or new page
- Allow `Hotkeys` to be configured in Settings
- Clean up redundant components
