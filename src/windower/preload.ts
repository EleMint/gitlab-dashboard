import { contextBridge } from 'electron'

contextBridge.exposeInMainWorld('electron', {
  GITLAB_INSTANCE: process.env.GITLAB_INSTANCE,
  GITLAB_PAT: process.env.GITLAB_PAT,
  GITLAB_DASHBOARD_IS_DEVELOPMENT: process.env.GITLAB_DASHBOARD_IS_DEVELOPMENT
})
