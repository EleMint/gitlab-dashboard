export type TimeFormat = string

const difference = (previous: Date | string, current?: Date | string) => {
  const then = typeof previous === 'string' ? new Date(previous) : previous

  const now = current
    ? typeof current === 'string'
      ? new Date(current)
      : current
    : new Date()

  const msMinute = 60 * 1000
  const msHour = msMinute * 60
  const msDay = msHour * 24
  const msMonth = msDay * 30
  const msYear = msDay * 365

  const elapsed = now.valueOf() - then.valueOf()

  if (elapsed < msMinute) {
    const rounded = Math.round(elapsed / 1000)
    if (rounded === 1) return 'a second ago'
    return `${rounded} seconds ago`
  } else if (elapsed < msHour) {
    const rounded = Math.round(elapsed / msMinute)
    if (rounded === 1) return 'a minute ago'
    return `${rounded} minutes ago`
  } else if (elapsed < msDay) {
    const rounded = Math.round(elapsed / msHour)
    if (rounded === 1) return 'an hour ago'
    return `${rounded} hours ago`
  } else if (elapsed < msMonth) {
    const rounded = Math.round(elapsed / msDay)
    if (rounded === 1) return 'a day ago'
    return `${rounded} days ago`
  } else if (elapsed < msYear) {
    const rounded = Math.round(elapsed / msMonth)
    if (rounded === 1) return 'a month ago'
    return `${rounded} months ago`
  }

  const rounded = Math.round(elapsed / msYear)
  if (rounded === 1) return `a year ago`
  return `${rounded} years ago`
}

const formatter = new Intl.DateTimeFormat('en', {
  year: 'numeric',
  month: 'short',
  day: 'numeric',
  hour12: true,
  hour: 'numeric',
  minute: '2-digit'
})

const format = (date: Date | string) => {
  const now = typeof date === 'string' ? new Date(date) : date

  return formatter.format(now)
}

export const date = {
  difference,
  format
}
