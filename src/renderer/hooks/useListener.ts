import React from 'react'

const useListener = (
  type: keyof WindowEventMap,
  listener: (this: Window, ev: Event) => void
) => {
  React.useEffect(() => {
    window.addEventListener(type, listener)

    return () => window.removeEventListener(type, listener)
  })
}

export default useListener
