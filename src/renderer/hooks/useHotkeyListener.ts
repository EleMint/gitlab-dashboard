import useListener from './useListener'

import { Hotkey } from '../providers/config/values/hotkeys'

const useHotkeyListener = (
  hotkey: Hotkey,
  listener: (ev: KeyboardEvent) => void
) => {
  const _listener = (ev: KeyboardEvent) => {
    if (hotkey.meta) {
      if (hotkey.meta.altKey && !ev.altKey) return

      if (hotkey.meta.ctrlKey && !ev.ctrlKey) return
    }

    if (hotkey.key !== ev.key) return

    listener(ev)
  }

  useListener('keypress', _listener)
}

export default useHotkeyListener
