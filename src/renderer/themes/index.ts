import dark from './dark'

export enum Theme {
  dark = 'dark'
}

const themes = {
  [Theme.dark]: dark
}

export default themes
