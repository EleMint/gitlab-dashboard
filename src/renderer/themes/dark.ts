import { DefaultTheme } from 'styled-components'

const dark: DefaultTheme = {
  background: {
    primary: '#1f1f1f',
    secondary: '#404040'
  },
  color: {
    primary: '#fafafa',
    secondary: '#999999',
    error: '#ec5941',
    warning: '#ff9966'
  }
}

export default dark
