import { ApolloError, gql, useQuery } from '@apollo/client'

import { MergeRequestState } from '../../shared/merge-request/state'

interface QUERY_VARIABLES {
  draft: boolean
  fullPath: string
  state: MergeRequestState
}

const QUERY = gql`
  query Project($fullPath: ID!, $draft: Boolean, $state: MergeRequestState) {
    project(fullPath: $fullPath) {
      id
      fullPath
      avatarUrl
      name
      webUrl
      mergeRequests(draft: $draft, state: $state) {
        nodes {
          id
          title
          state
          createdAt
          updatedAt
          reference(full: true)
          webUrl
          author {
            webUrl
            username
          }
        }
      }
    }
  }
`

export interface Project {
  id: string
  fullPath: string
  avatarUrl?: string
  name: string
  webUrl: string
  mergeRequests: {
    nodes: MergeRequest[]
  }
}

export interface MergeRequest {
  id: string
  title: string
  state: MergeRequestState
  createdAt: string
  updatedAt: string
  reference: string
  webUrl: string
  author: Author
}

export interface Author {
  username: string
  webUrl: string
}

export interface Data {
  project: Project
}

interface UseProjectQuery {
  loading: boolean
  error: ApolloError
  data: Data
}

const useProjectQuery = (
  variables: QUERY_VARIABLES,
  pollInterval: number
): UseProjectQuery => {
  const { loading, error, data } = useQuery(QUERY, {
    variables,
    pollInterval
  })

  return { loading, error, data }
}

export default useProjectQuery
