import React from 'react'

import useConfig from '../../providers/hooks/useConfig'
import { ViewNavigation } from '../../providers/view'

import Error from '../shared/error'
import Loading from '../shared/loading'

import CopyText from '../shared/copy-text'
import MR from '../shared/merge-request/mr'
import { MergeRequestState, MRStateRadios } from '../shared/merge-request/state'
import RadioGroup from '../shared/radio-group'

import Container from './components/container'
import Column from './components/column'
import ErrorWrapper from './components/error-wrapper'
import LoadingWrapper from './components/loading-wrapper'
import Image from './components/image'
import Title from './components/title'
import Header from './components/header'
import Name from './components/name'
import Path from './components/path'
import SubHeader from './components/subheader'
import List from './components/list'
import ListItem from './components/list-item'

import useProjectQuery from './graphql/project-mrs'
import Toggle from '../shared/toggle'

export interface ProjectViewMetadata {
  id: string
  fullPath: string
  state?: MergeRequestState
}

type ProjectProps = ViewNavigation & ProjectViewMetadata

const Project: React.FC<ProjectProps> = ({ fullPath, state }) => {
  const { pollInterval } = useConfig()

  const [mrState, setMRState] = React.useState<MergeRequestState>(
    state ?? MergeRequestState.OPENED
  )

  const [draft, setDraft] = React.useState(false)

  const mrStateLabel = React.useMemo(
    () => MRStateRadios.find(state => state.value === mrState).label,
    [mrState]
  )

  const { loading, error, data } = useProjectQuery(
    {
      fullPath,
      draft,
      state: mrState
    },
    pollInterval
  )

  if (loading)
    return (
      <LoadingWrapper>
        <Loading />
      </LoadingWrapper>
    )

  if (error)
    return (
      <ErrorWrapper>
        <Error error={error} />
      </ErrorWrapper>
    )

  const { project } = data
  const {
    mergeRequests: { nodes: mergeRequests }
  } = project

  return (
    <Container>
      <Title>
        {project.avatarUrl && (
          <Column direction='row'>
            <Column>
              <Image src={project.avatarUrl} />
            </Column>
          </Column>
        )}
        <Column>
          <Header>
            <Name onClick={() => window.open(project.webUrl, '_blank')}>
              {project.name}
            </Name>
            <Path>{project.fullPath}</Path>
          </Header>
          <SubHeader>
            Project ID: <CopyText value={project.id.split('/').pop()} />
          </SubHeader>
        </Column>
        <Column
          direction='column'
          align='end'
          justify='space-evenly'
          fullHeight
        >
          <Toggle enabled={draft} setEnabled={setDraft} label='Draft' />
          <RadioGroup
            radios={MRStateRadios}
            selected={mrState}
            setSelected={(selected: MergeRequestState) => setMRState(selected)}
          />
        </Column>
      </Title>
      <List>
        {!mergeRequests.length && (
          <ListItem warning>No {mrStateLabel} Merge Requests</ListItem>
        )}

        {mergeRequests.map(mr => (
          <ListItem key={mr.id}>
            <MR {...mr} />
          </ListItem>
        ))}
      </List>
    </Container>
  )
}

export default Project
