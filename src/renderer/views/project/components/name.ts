import styled from 'styled-components'

const Name = styled.div`
  color: ${props => props.theme.color.primary};
  user-select: none;

  :hover {
    text-decoration: underline;
    text-decoration-color: ${props => props.theme.color.secondary};
    cursor: pointer;
  }
`

export default Name
