import styled from 'styled-components'

const Path = styled.span`
  color: ${props => props.theme.color.secondary};
  font-style: italic;
  font-size: 0.875rem;
  font-weight: 400;
  line-height: 1rem;
`

export default Path
