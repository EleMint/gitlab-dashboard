import styled from 'styled-components'

const Image = styled.img`
  width: 75px;
  height: 75px;
  margin-right: 1.5rem;
`

export default Image
