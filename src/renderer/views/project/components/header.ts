import styled from 'styled-components'

const Header = styled.h4`
  color: ${props => props.theme.color.primary};
  font-size: 1.25rem;
  font-weight: 600;
  line-height: 1.25rem;
  user-select: none;

  :hover {
    text-decoration: underline;
    text-decoration-color: ${props => props.theme.color.secondary};
    cursor: pointer;
  }
`

export default Header
