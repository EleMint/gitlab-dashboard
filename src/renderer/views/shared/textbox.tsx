import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.span<{ maxWidth?: number }>`
  position: relative;
  display: inline-flex;
  width: 100%;
  ${props => (props.maxWidth ? `max-width: ${props.maxWidth}rem;` : '')}
`
const Input = styled.input`
  background-color: #333333;
  appearance: none;
  border-radius: 0.25rem;
  border-color: #868686;
  border-style: none;
  height: 1.5rem;
  line-height: 1rem;
  font-size: 1rem;
  width: 100%;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  padding-left: 0.75rem;
  padding-right: 0.75rem;
  outline: none;
  overflow-x: visible;
  color: ${props => props.theme.color.primary};
`

interface TextboxProps {
  type?: React.HTMLInputTypeAttribute
  value?: string
  onChange?: React.ChangeEventHandler<HTMLInputElement>
  maxWidth?: number
}

const Textbox: React.FC<TextboxProps> = ({
  type,
  value,
  onChange,
  maxWidth
}) => (
  <Wrapper maxWidth={maxWidth}>
    <Input type={type} value={value} onChange={onChange} />
  </Wrapper>
)

export default Textbox
