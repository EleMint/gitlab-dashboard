import React from 'react'
import styled from 'styled-components'

import { date } from '../../utils/date'

const Container = styled.span``

interface DateTimeProps {
  value: string
}

const DateTime: React.FC<DateTimeProps> = ({ value }) => (
  <Container title={date.format(value)}>{date.difference(value)}</Container>
)

export default DateTime
