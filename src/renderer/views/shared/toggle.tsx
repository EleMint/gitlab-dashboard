import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`

const Switch = styled.label`
  position: relative;
  display: inline-block;
  width: 4rem;
  height: 2rem;
  border: 1px solid ${props => props.theme.background.secondary};
  border-radius: 34px;

  input {
    opacity: 0;
    width: 0;
    height: 0;
  }

  input:checked + span {
    background-color: ${props => props.theme.background.primary};
  }

  input:checked + span:before {
    transform: translateX(26px);
  }
`

const Input = styled.input`
  :checked {
    background-color: ${props => props.theme.background.primary};
    transform: translateX(26px);
  }
`

const Slider = styled.span`
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: ${props => props.theme.color.primary};
  transition: 0.4s;
  border-radius: 34px;
  background-color: ${props => props.theme.background.secondary};

  :before {
    position: absolute;
    content: '';
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    transition: 0.4s;
    border-radius: 50%;
  }
`

const Label = styled.span`
  margin: 1rem;
`

interface ToggleProps {
  enabled: boolean
  setEnabled: (enabled: boolean) => void
  label: string
}

const Toggle: React.FC<ToggleProps> = ({ enabled, setEnabled, label }) => (
  <Wrapper>
    <Switch>
      <Input
        type='checkbox'
        checked={enabled}
        onChange={e => setEnabled(e.target.checked)}
      />
      <Slider />
    </Switch>
    {label && <Label>{label}</Label>}
  </Wrapper>
)

export default Toggle
