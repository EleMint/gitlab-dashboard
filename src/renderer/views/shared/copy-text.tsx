import React from 'react'
import styled from 'styled-components'

interface LinkProps {
  copied?: boolean
}

const Link = styled.a<LinkProps>`
  color: ${props =>
    props.copied ? props.theme.color.secondary : props.theme.color.primary};

  :hover {
    color: ${props => props.theme.color.secondary};
    cursor: pointer;
  }
`

const Message = styled.span`
  color: ${props => props.theme.color.secondary};
  font-style: italic;
`

interface CopyTextProps {
  tooltip?: string
  value: string | number
}

const CopyText: React.FC<CopyTextProps> = ({ tooltip, value }) => {
  const [copied, setCopied] = React.useState(false)

  const stringified = String(value)

  const onClick = async () => {
    await navigator.clipboard.writeText(stringified)
    setCopied(true)
  }

  return (
    <>
      <Link
        copied={copied}
        onClick={onClick}
        title={tooltip ? tooltip : 'Click to copy'}
      >
        {value}
      </Link>
      <Message hidden={!copied}>{' - Copied'}</Message>
    </>
  )
}

export default CopyText
