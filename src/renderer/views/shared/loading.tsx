import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.span``

const Loading: React.FC = () => <Wrapper>Loading...</Wrapper>

export default Loading
