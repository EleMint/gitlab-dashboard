import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.span`
  position: relative;
  display: inline-flex;
  width: 100%;
`

const Input = styled.input`
  background-color: #333333;
  appearance: none;
  background-clip: padding-box;
  border-radius: 0.25rem;
  border-color: #868686;
  border-style: none;
  height: 1.5rem;
  line-height: 1rem;
  font-size: 1rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  padding-left: 0.75rem;
  padding-right: 2.4rem;
  outline: none;
  overflow-x: visible;
  color: ${props => props.theme.color.primary};
  width: 100%;
`

const Eye = styled.span`
  height: 57%;
  position: absolute;
  right: 0.75rem;
  top: 45%;
  transform: translateY(-40%);

  svg {
    filter: invert(0.98);

    :hover {
      cursor: pointer;
    }
  }
`

interface HiddenInputProps {
  value?: string
  onChange?: React.ChangeEventHandler<HTMLInputElement>
}

const HiddenInput: React.FC<HiddenInputProps> = ({ value, onChange }) => {
  const [isVisible, setIsVisible] = React.useState(false)

  const onClick = () => {
    setIsVisible(!isVisible)
  }

  return (
    <Wrapper>
      <Input
        type={isVisible ? 'text' : 'password'}
        value={value}
        onChange={onChange}
      />
      <Eye onClick={onClick}>
        <svg
          xmlns='http://www.w3.org/2000/svg'
          width='24'
          height='24'
          viewBox='0 0 24 24'
        >
          <path d='M12.015 7c4.751 0 8.063 3.012 9.504 4.636-1.401 1.837-4.713 5.364-9.504 5.364-4.42 0-7.93-3.536-9.478-5.407 1.493-1.647 4.817-4.593 9.478-4.593zm0-2c-7.569 0-12.015 6.551-12.015 6.551s4.835 7.449 12.015 7.449c7.733 0 11.985-7.449 11.985-7.449s-4.291-6.551-11.985-6.551zm-.015 3c-2.209 0-4 1.792-4 4 0 2.209 1.791 4 4 4s4-1.791 4-4c0-2.208-1.791-4-4-4z' />
        </svg>
      </Eye>
    </Wrapper>
  )
}

export default HiddenInput
