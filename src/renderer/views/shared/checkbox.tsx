import React, { DetailedHTMLProps, InputHTMLAttributes } from 'react'

type InputProps = Omit<
  DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>,
  'type'
>

const Checkbox = (props: InputProps) => <input type='checkbox' {...props} />

export default Checkbox
