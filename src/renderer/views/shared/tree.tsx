import React from 'react'
import { DefaultTheme, StyledComponent } from 'styled-components'

import Checkbox from './checkbox'

type IID<T = object> = T & {
  id: string
}

export type Node<T = object, U = object> = IID<T> & {
  leafs?: Leaf<U>[]
}

export type Leaf<U = object> = IID<U>

interface TreeProps<T = object, U = object> {
  nodes: Node<T, U>[]

  selectable?: boolean
  checkedNodes?: IID['id'][]
  checkedLeafs?: IID['id'][]
  setCheckedNodes?: (checkedNodes: IID['id'][]) => void
  setCheckedLeafs?: (checkedLeafs: IID['id'][]) => void

  NodeComponent: React.FC<Node<T, U>>
  LeafComponent: React.FC<Leaf<U>>

  Row: StyledComponent<
    'div',
    DefaultTheme,
    {
      marginLeft?: number
      flex?: 'left' | 'right'
      onClick?: React.MouseEventHandler
    },
    never
  >
  Column: StyledComponent<'span', DefaultTheme, Record<string, unknown>, never>

  collapsible?: boolean
}

const Tree: React.FC<TreeProps<object, object>> = ({
  nodes,
  checkedNodes,
  checkedLeafs,
  setCheckedNodes,
  setCheckedLeafs,
  NodeComponent,
  LeafComponent,
  Row,
  Column,
  selectable = true
}) => {
  const onChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    node: Node,
    leaf?: Leaf
  ) => {
    const checked = e.target.checked

    if (leaf) {
      const alreadyChecked = checkedLeafs.includes(leaf.id)

      if (checked && !alreadyChecked) {
        const tmp = checkedLeafs.slice()
        tmp.push(leaf.id)
        setCheckedLeafs(tmp)
      } else if (!checked && alreadyChecked) {
        const filtered = checkedLeafs.filter(id => id !== leaf.id)
        setCheckedLeafs(filtered)
      }

      let allLeafsUnchecked = true
      node.leafs.forEach(l => {
        if (l.id === leaf.id && checked) {
          allLeafsUnchecked = false
        } else if (checkedLeafs.includes(l.id) && l.id !== leaf.id) {
          allLeafsUnchecked = false
        }
      })

      const nodeChecked = checkedNodes.includes(node.id)

      if (checked && !nodeChecked) {
        const tmp = checkedNodes.slice()
        tmp.push(node.id)
        setCheckedNodes(tmp)
      } else if (!checked && allLeafsUnchecked && nodeChecked) {
        const filtered = checkedNodes.filter(id => id !== node.id)
        setCheckedNodes(filtered)
      }
    } else {
      const alreadyChecked = checkedNodes.includes(node.id)
      const leafIds = node.leafs.map(l => l.id)

      if (checked && !alreadyChecked) {
        const tmpNodes = checkedNodes.slice()
        tmpNodes.push(node.id)
        setCheckedNodes(tmpNodes)

        const tmpLeafs = checkedLeafs.slice()
        const notCheckedLeafIds = leafIds.filter(
          id => !checkedLeafs.includes(id)
        )
        tmpLeafs.push(...notCheckedLeafIds)
        setCheckedLeafs(tmpLeafs)
      } else if (!checked && alreadyChecked) {
        const filteredNodes = checkedNodes.filter(id => id !== node.id)
        setCheckedNodes(filteredNodes)

        const filteredLeafs = checkedLeafs.filter(id => !leafIds.includes(id))
        setCheckedLeafs(filteredLeafs)
      }
    }
  }

  return (
    <>
      {nodes.map(node => (
        <React.Fragment key={node.id}>
          <Row marginLeft={1} flex='left'>
            <Column>
              {selectable && (
                <Checkbox
                  checked={checkedNodes.includes(node.id)}
                  onChange={e => onChange(e, node)}
                />
              )}
            </Column>
            <Column>
              <NodeComponent {...node} />
            </Column>
          </Row>
          {!!node.leafs &&
            !!node.leafs.length &&
            node.leafs.map(leaf => (
              <Row key={leaf.id} marginLeft={2}>
                <Column>
                  {selectable && (
                    <Checkbox
                      checked={checkedLeafs.includes(leaf.id)}
                      onChange={e => onChange(e, node, leaf)}
                    />
                  )}
                  <LeafComponent {...leaf} />
                </Column>
              </Row>
            ))}
        </React.Fragment>
      ))}
    </>
  )
}

export default Tree
