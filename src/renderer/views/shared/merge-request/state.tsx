export enum MergeRequestState {
  OPENED = 'opened',
  CLOSED = 'closed',
  LOCKED = 'locked',
  MERGED = 'merged'
}

export const MRStateRadios = [
  {
    label: 'Closed',
    value: MergeRequestState.CLOSED
  },
  {
    label: 'Locked',
    value: MergeRequestState.LOCKED
  },
  {
    label: 'Merged',
    value: MergeRequestState.MERGED
  },
  {
    label: 'Opened',
    value: MergeRequestState.OPENED
  }
]
