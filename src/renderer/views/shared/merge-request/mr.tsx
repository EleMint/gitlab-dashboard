import React from 'react'
import styled from 'styled-components'

import DateTime from '../date-time'

const Container = styled.div`
  display: flex;
  flex-direction: column;
`

const TitleWrapper = styled.div``

const TitleLink = styled.a`
  color: ${props => props.theme.color.primary};
  text-decoration: none;

  :hover {
    color: ${props => props.theme.color.secondary};
  }
`

const Title: React.FC<{ href: string }> = ({ children, href }) => (
  <TitleWrapper>
    <TitleLink href={href} target='_blank'>
      {children}
    </TitleLink>
  </TitleWrapper>
)

const Description = styled.div`
  color: ${props => props.theme.color.secondary};
`

const Username = styled.a`
  color: ${props => props.theme.color.primary};
  text-decoration: none;

  :hover {
    color: ${props => props.theme.color.secondary};
  }
`

const State = styled.div``

interface MRProps {
  title: string
  state: string
  webUrl: string
  reference: string
  createdAt: string
  updatedAt: string
  author: {
    username: string
    webUrl: string
  }
}

const MR: React.FC<MRProps> = ({
  title,
  state,
  webUrl,
  reference,
  createdAt,
  updatedAt,
  author
}) => (
  <>
    <Container>
      <Title href={webUrl}>{title}</Title>
      <Description>
        {reference}
        {' created '}
        <DateTime value={createdAt} />
        {' by '}
        <Username href={author.username} target='_blank'>
          {author.username}
        </Username>
      </Description>
    </Container>
    <Container>
      <State>{state}</State>
      <Description>
        {'updated '}
        <DateTime value={updatedAt} />
      </Description>
    </Container>
  </>
)

export default MR
