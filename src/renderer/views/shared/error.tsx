import React from 'react'
import { ApolloError } from '@apollo/client'
import styled from 'styled-components'

const Wrapper = styled.span``

interface ErrorProps {
  error: ApolloError
}

const Error: React.FC<ErrorProps> = ({ error }) => (
  <Wrapper>{JSON.stringify(error, undefined, 2)}</Wrapper>
)

export default Error
