import styled from 'styled-components'

interface ButtonProps {
  primary?: boolean
  disabled?: boolean
  marginRight?: number
}

const Button = styled.button<ButtonProps>`
  display: flex;
  background-color: ${props =>
    props.primary ? '#5ca1e6' : props.theme.background.secondary};
  border-color: #404040;
  color: ${props => (props.primary ? '#333333' : props.theme.color.primary)};
  border-style: solid;
  border-width: 1px;
  border-radius: 0.25rem;
  appearance: none;
  outline: none;
  overflow-x: visible;
  padding: 0.5rem 0.75rem;
  margin-right: ${props => props.marginRight ?? 0}rem;
  align-items: center;
  justify-content: center;
  text-align: center;

  :hover {
    cursor: ${props => (props.disabled ? 'not-allowed' : 'pointer')};
  }
`

export default Button
