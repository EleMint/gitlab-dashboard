import React from 'react'
import styled from 'styled-components'

interface RadioProps {
  label?: string
  value: string
  selected?: boolean
  disabled?: boolean
}

interface RadioGroupProps {
  radios: RadioProps[]
  selected: string
  setSelected: (selected: string) => void
}

const Wrapper = styled.span`
  display: flex;
  flex-direction: row;
`

const Button = styled.button<RadioProps>`
  height: 2rem;
  display: flex;
  background-color: ${props =>
    props.selected
      ? props.theme.background.secondary
      : props.theme.background.primary};
  border-color: #404040;
  color: ${props => props.theme.color.primary};
  border-style: solid;
  border-width: 1px;
  border-radius: 0.25rem;
  appearance: none;
  outline: none;
  overflow-x: visible;
  padding: 0.5rem 0.75rem;
  align-items: center;
  justify-content: center;
  text-align: center;

  :hover {
    cursor: ${props => (props.disabled ? 'not-allowed' : 'pointer')};
  }
`

const RadioGroup: React.FC<RadioGroupProps> = ({
  radios,
  selected,
  setSelected
}) => {
  const onClick = (
    radio: RadioProps,
    e: React.MouseEvent<HTMLButtonElement>
  ) => {
    e.preventDefault()

    if (radio.disabled) return

    setSelected(radio.value)
  }

  return (
    <Wrapper>
      {radios.map(radio => (
        <Button
          key={radio.value}
          selected={selected === radio.value}
          value={radio.value}
          disabled={radio.disabled}
          onClick={e => onClick(radio, e)}
        >
          {radio.label}
        </Button>
      ))}
    </Wrapper>
  )
}
export default RadioGroup
