import React from 'react'

import useView from '../providers/hooks/useView'
import { ViewNavigation } from '../providers/view'

import Group, { GroupViewMetadata } from './group'
import Main from './main'
import Project, { ProjectViewMetadata } from './project'
import Settings from './settings'
import Sidebar from './sidebar'

export enum View {
  MAIN,
  SETTINGS,
  GROUP,
  PROJECT
}

export type ViewMetadata = GroupViewMetadata | ProjectViewMetadata

type Views = Record<View, React.FC<ViewNavigation & ViewMetadata>>
const Views: Views = {
  [View.MAIN]: Main,
  [View.SETTINGS]: Settings,
  [View.GROUP]: Group,
  [View.PROJECT]: Project
}

const Base: React.FC = () => {
  const { navigation, current, currentMetadata } = useView()

  const Component = Views[current]

  return (
    <>
      <Sidebar {...navigation} {...currentMetadata} />
      <Component {...navigation} {...currentMetadata} />
    </>
  )
}

export default Base
