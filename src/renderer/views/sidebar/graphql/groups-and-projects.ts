import React from 'react'
import { gql, useQuery } from '@apollo/client'

import useConfig from '../../../providers/hooks/useConfig'

const QUERY = gql`
  query Sidebar {
    currentUser {
      groups {
        nodes {
          id
          name
          fullPath
          projects(includeSubgroups: true) {
            nodes {
              id
              name
              fullPath
            }
          }
        }
      }
    }
  }
`

export interface User {
  groups: {
    nodes: Group[]
  }
}

export interface Group {
  id: string
  name: string
  fullPath: string
  projects: {
    nodes: Project[]
  }
}

export interface Project {
  id: string
  name: string
  fullPath: string
}

export interface Data {
  currentUser: User
}

const useGroupsAndProjects = (): Group[] => {
  const { includeGroups, includeProjects } = useConfig()

  const [groups, setGroups] = React.useState<Group[]>([])

  const { loading, error, data, refetch } = useQuery<Data>(QUERY, {
    pollInterval: 0
  })

  React.useEffect(() => {
    refetch()
  }, [JSON.stringify(includeGroups), JSON.stringify(includeProjects)])

  React.useEffect(() => {
    if (!loading && error) {
      setGroups([])
      alert(error)
      return
    }

    if (!loading && data && data.currentUser) {
      const _groups = data.currentUser.groups.nodes
      setGroups(_groups)
      return
    }
  }, [loading])

  return groups
}

export default useGroupsAndProjects
