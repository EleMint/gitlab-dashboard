import React from 'react'

import { ViewNavigation } from '../../../providers/view'

import { View } from '../..'
import { ProjectViewMetadata } from '../../project'
import { Leaf } from '../../shared/tree'

import { Project } from '../graphql/groups-and-projects'

import Label from './label'

interface LeafComponentProps {
  current: ViewNavigation['current']
  currentMetadata?: ViewNavigation['currentMetadata']
  navigate: ViewNavigation['navigate']
}

const LeafComponent: (props: LeafComponentProps) => React.FC<Leaf<Project>> = ({
  current,
  currentMetadata,
  navigate
}) => ({ id, name, fullPath }) => {
  const isSelected =
    current === View.PROJECT &&
    currentMetadata &&
    (currentMetadata as ProjectViewMetadata).id === id

  const onClick = () => {
    const metadata: ProjectViewMetadata = {
      id,
      fullPath
    }

    navigate(View.PROJECT, metadata)
  }

  return (
    <Label primary={false} selected={isSelected} onClick={onClick}>
      {name}
    </Label>
  )
}

export default LeafComponent
