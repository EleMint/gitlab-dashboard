import styled from 'styled-components'

interface ContainerProps {
  hidden: boolean
  width: number
  flex?: 'end'
}

const Container = styled.div<ContainerProps>`
  display: ${({ hidden }) => (hidden ? 'none' : 'flex')};
  flex-direction: column;
  justify-content: ${props => props.flex ?? 'space-between'};
  width: 100%;
  max-width: ${props => props.width}%;
  height: 100vh;
  background-color: ${props => props.theme.background.secondary};
  color: ${props => props.theme.color.primary};
  overflow: hidden;
`

export default Container
