import styled from 'styled-components'

interface RowProps {
  marginLeft?: number
  flex?: 'left' | 'right'
}

const justifyContent = (value?: 'left' | 'right'): string => {
  if (value === 'left') return 'start'
  if (value === 'right') return 'end'
  return 'space-between'
}

const Row = styled.div<RowProps>`
  display: flex;
  flex-direction: row;
  justify-content: ${props => justifyContent(props.flex)};
  color: ${props => props.theme.color.primary};
  margin-top: 0.5rem;
  margin-bottom: 0.5rem;
  margin-left: ${props => props.marginLeft ?? 1.5}rem;
  margin-right: 1.5rem;
`

export default Row
