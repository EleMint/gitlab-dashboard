import React from 'react'
import styled from 'styled-components'

interface BorderProps {
  onDoubleClick?: React.MouseEventHandler
}

const Border = styled.div`
  display: flex;
  height: 100vh;
  align-items: center;
  color: ${props => props.theme.color.primary};
  background-color: ${props => props.theme.background.secondary};
  user-select: none;
  padding-left: 0.125rem;
  padding-right: 0.125rem;

  :hover {
    cursor: col-resize;
  }
`

const DraggableBorder: React.FC<BorderProps> = ({ onDoubleClick }) => (
  <Border onDoubleClick={onDoubleClick}>||</Border>
)

export default DraggableBorder
