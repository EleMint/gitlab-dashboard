import React from 'react'

import { ViewNavigation } from '../../../providers/view'

import { View } from '../..'
import { GroupViewMetadata } from '../../group'
import { Node } from '../../shared/tree'

import { Group, Project } from '../graphql/groups-and-projects'

import Label from './label'

interface NodeComponentProps {
  current: ViewNavigation['current']
  currentMetadata?: ViewNavigation['currentMetadata']
  navigate: ViewNavigation['navigate']
}

const NodeComponent: (
  props: NodeComponentProps
) => React.FC<Node<Group, Project>> = ({
  current,
  currentMetadata,
  navigate
}) => ({ id, name, fullPath }) => {
  const isSelected =
    current === View.GROUP &&
    currentMetadata &&
    (currentMetadata as GroupViewMetadata).id === id

  const onClick = () => {
    const metadata: GroupViewMetadata = {
      id,
      fullPath
    }

    navigate(View.GROUP, metadata)
  }

  return (
    <Label onClick={onClick} primary selected={isSelected}>
      {name}
    </Label>
  )
}

export default NodeComponent
