import styled, { DefaultTheme } from 'styled-components'

interface StandaloneLabelProps {
  primary?: boolean
  error?: boolean
  selected?: boolean
}

const getColor = (
  theme: DefaultTheme,
  selected = false,
  error = false
): string => {
  if (selected) return `color: ${theme.color.primary};`
  if (error) return `color: ${theme.color.error};`
  return `color: ${theme.color.secondary};`
}

const StandaloneLabel = styled.span<StandaloneLabelProps>`
  ${({ theme, selected, error }) => getColor(theme, selected, error)}
  font-size: ${props => (props.primary ? 1 : 0.9)}rem;
  user-select: none;
  cursor: pointer;
  margin: 0.5rem 1.5rem 0.5rem 1rem;
`

export default StandaloneLabel
