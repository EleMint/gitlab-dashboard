import React from 'react'
import { View } from '../..'
import Container from './container'
import Section from './section'
import StandaloneLabel from './standalone-label'

interface NoPATProps {
  sidebar: boolean
  sidebarWidth: number
  current: View
  navigate: (view: View) => void
}

const NoPAT: React.FC<NoPATProps> = ({
  sidebar,
  sidebarWidth,
  current,
  navigate
}) => (
  <>
    <Container hidden={!sidebar} width={sidebarWidth} flex="end">
      <Section>
        <StandaloneLabel
          error
          primary
          onClick={() => current !== View.SETTINGS && navigate(View.SETTINGS)}
        >
          Settings
        </StandaloneLabel>
      </Section>
    </Container>
  </>
)

export default NoPAT
