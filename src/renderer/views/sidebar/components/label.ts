import styled from 'styled-components'

interface LabelProps {
  primary: boolean
  selected: boolean
}

const Label = styled.span<LabelProps>`
  color: ${props =>
    props.selected ? props.theme.color.primary : props.theme.color.secondary};
  font-size: ${props => (props.primary ? 1 : 0.9)}rem;
  user-select: none;
  cursor: pointer;
`

export default Label
