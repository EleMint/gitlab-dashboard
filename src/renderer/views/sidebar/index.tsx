import React from 'react'

import {
  IncludeGroupsType,
  IncludeProjectsType
} from '../../providers/config/values'
import useConfig from '../../providers/hooks/useConfig'
import useHotkeyListener from '../../hooks/useHotkeyListener'
import useView from '../../providers/hooks/useView'
import { ViewNavigation } from '../../providers/view'

import { View } from '..'

import Tree, { Node } from '../shared/tree'

import useGroupsAndProjects, {
  Group,
  Project
} from './graphql/groups-and-projects'

import Container from './components/container'
import DraggableBorder from './components/draggable-border'
import Column from './components/column'
import Row from './components/row'
import Section from './components/section'
import LeafComponent from './components/leaf'
import NodeComponent from './components/node'
import StandaloneLabel from './components/standalone-label'
import NoPAT from './components/no-pat'

const filterGroups = (
  groups: Group[],
  includeGroups: IncludeGroupsType,
  includeProjects: IncludeProjectsType
): Node<Group, Project>[] =>
  groups
    .filter(group => includeGroups.includes(group.id))
    .map(group => ({
      ...group,
      leafs: group.projects.nodes.filter(project =>
        includeProjects.includes(project.id)
      )
    }))

const Sidebar: React.FC<ViewNavigation> = ({
  current,
  currentMetadata,
  navigate,
  sidebarWidth
}) => {
  const { hotkeys, includeGroups, includeProjects, pat } = useConfig()
  const toggleSidebarHotkey = hotkeys.VIEW.TOGGLE_SIDEBAR

  const { sidebar, toggleSidebar } = useView()

  useHotkeyListener(toggleSidebarHotkey, toggleSidebar)

  if (!pat)
    return (
      <NoPAT
        sidebar={sidebar}
        sidebarWidth={sidebarWidth}
        current={current}
        navigate={navigate}
      />
    )

  const groups = useGroupsAndProjects()

  const nodes = React.useMemo(
    () => filterGroups(groups, includeGroups, includeProjects),
    [
      JSON.stringify(groups),
      JSON.stringify(includeGroups),
      JSON.stringify(includeProjects)
    ]
  )

  return (
    <>
      <Container hidden={!sidebar} width={sidebarWidth}>
        <Section>
          <StandaloneLabel
            primary
            selected={current === View.MAIN}
            onClick={() => current !== View.MAIN && navigate(View.MAIN)}
          >
            Merge Requests
          </StandaloneLabel>
          <Tree
            nodes={nodes}
            NodeComponent={NodeComponent({
              current,
              currentMetadata,
              navigate
            })}
            LeafComponent={LeafComponent({
              current,
              currentMetadata,
              navigate
            })}
            Column={Column}
            Row={Row}
            selectable={false}
          />
        </Section>
        <Section>
          <StandaloneLabel
            primary
            selected={current === View.SETTINGS}
            onClick={() => current !== View.SETTINGS && navigate(View.SETTINGS)}
          >
            Settings
          </StandaloneLabel>
        </Section>
      </Container>
      <DraggableBorder onDoubleClick={toggleSidebar} />
    </>
  )
}

export default Sidebar
