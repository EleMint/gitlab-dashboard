import React from 'react'

import HiddenInput from '../../shared/hidden-input'

import Column from '../components/column'
import Row from '../components/row'

interface PersonalAccessTokenProps {
  pat: string
  setPAT: (value: React.SetStateAction<string>) => void
}

const PersonalAccessToken: React.FC<PersonalAccessTokenProps> = ({
  pat,
  setPAT
}) => (
  <Row>
    <Column
      noHighlight
      title='GitLab Personal Access Token requires `read_api` and `read_user` permissions.'
    >
      Personal Access Token
    </Column>
    <Column flex='right' fullWidth maxWidth={18}>
      <HiddenInput value={pat} onChange={e => setPAT(e.target.value)} />
    </Column>
  </Row>
)

export default PersonalAccessToken
