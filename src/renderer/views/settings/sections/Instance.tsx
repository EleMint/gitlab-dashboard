import React from 'react'

import Textbox from '../../shared/textbox'

import Column from '../components/column'
import Row from '../components/row'

interface InstanceProps {
  instance: string
  setInstance: (value: React.SetStateAction<string>) => void
}

const Instance: React.FC<InstanceProps> = ({ instance, setInstance }) => (
  <Row>
    <Column
      noHighlight
      title='URL used to access your GitLab instance. Defaults to `https://gitlab.com`.'
    >
      GitLab Instance
    </Column>
    <Column flex='right' fullWidth maxWidth={18}>
      <Textbox
        type='url'
        value={instance}
        onChange={e => setInstance(e.target.value)}
        maxWidth={18}
      />
    </Column>
  </Row>
)

export default Instance
