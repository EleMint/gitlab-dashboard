import React from 'react'

import NumericTextbox from '../../shared/numeric-textbox'

import Column from '../components/column'
import Row from '../components/row'

interface PollIntervalProps {
  pollInterval: number
  setPollInterval: (value: React.SetStateAction<number>) => void
}

const PollInterval: React.FC<PollIntervalProps> = ({
  pollInterval,
  setPollInterval
}) => (
  <Row>
    <Column
      noHighlight
      title='Seconds between requests to GitLab. Defaults to `60` (1 min), set to `0` to disable.'
    >
      Poll Interval
    </Column>
    <Column flex='right' fullWidth maxWidth={18}>
      <NumericTextbox
        value={pollInterval / 1000}
        onChange={e => {
          const value = e.target.valueAsNumber
          const defaulted = Number.isNaN(value) ? 0 : value
          setPollInterval(defaulted * 1000)
        }}
      />
    </Column>
  </Row>
)

export default PollInterval
