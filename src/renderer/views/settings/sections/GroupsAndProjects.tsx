import React from 'react'
import { ApolloError } from '@apollo/client'
import styled from 'styled-components'

import Error from '../../shared/error'
import Loading from '../../shared/loading'
import Tree, { Node } from '../../shared/tree'

import ErrorWrapper from '../components/error-wrapper'
import LoadingWrapper from '../components/loading-wrapper'

import LeafComponent from '../components/leaf'
import NodeComponent from '../components/node'

import Column from '../components/column'
import Row from '../components/row'

import { Data, Group, Project } from '../graphql/groups-and-projects'

const Wrapper = styled.div`
  margin-left: 0.75rem;
`

interface GroupsAndProjectsProps {
  loading: boolean
  error: ApolloError
  data: Data
  includeGroups: string[]
  setIncludeGroups: (includeGroups: string[]) => void
  includeProjects: string[]
  setIncludeProjects: (includeProjects: string[]) => void
}

const GroupsAndProjects: React.FC<GroupsAndProjectsProps> = ({
  loading,
  error,
  data,
  includeGroups,
  setIncludeGroups,
  includeProjects,
  setIncludeProjects
}) => {
  if (loading)
    return (
      <LoadingWrapper>
        <Loading />
      </LoadingWrapper>
    )

  if (error)
    return (
      <ErrorWrapper>
        <Error error={error} />
      </ErrorWrapper>
    )

  if (!data.currentUser || !data.currentUser.groups.nodes.length)
    return (
      <ErrorWrapper>
        <span>No GitLab Data Available</span>
      </ErrorWrapper>
    )

  const nodes: Node<Group, Project>[] = data.currentUser.groups.nodes.map(
    group => ({
      ...group,
      leafs: group.projects.nodes.map(project => ({
        ...project
      }))
    })
  )

  return (
    <Wrapper>
      <Tree
        nodes={nodes}
        checkedNodes={includeGroups}
        checkedLeafs={includeProjects}
        setCheckedNodes={setIncludeGroups}
        setCheckedLeafs={setIncludeProjects}
        NodeComponent={NodeComponent}
        LeafComponent={LeafComponent}
        Column={Column}
        Row={Row}
      />
    </Wrapper>
  )
}

export default GroupsAndProjects
