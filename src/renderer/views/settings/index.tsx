import React from 'react'

import useConfig from '../../providers/hooks/useConfig'
import { ViewNavigation } from '../../providers/view'

import useGroupsAndProjects from './graphql/groups-and-projects'

import ButtonGroup from './components/button-group'
import Container from './components/container'
import Header from './components/header'
import Section from './components/section'

import Instance from './sections/Instance'
import PersonalAccessToken from './sections/PersonalAccessToken'
import PollInterval from './sections/PollInterval'
import GroupsAndProjects from './sections/GroupsAndProjects'

const Settings: React.FC<ViewNavigation> = ({ back }) => {
  const config = useConfig()

  const [instance, setInstance] = React.useState(config.instance)
  const [pat, setPAT] = React.useState(config.pat)
  const [pollInterval, setPollInterval] = React.useState(config.pollInterval)
  const [includeGroups, setIncludeGroups] = React.useState(config.includeGroups)
  const [includeProjects, setIncludeProjects] = React.useState(
    config.includeProjects
  )

  const { loading, error, data } = useGroupsAndProjects()

  const onCancel = () => {
    back()
  }

  const onSave = () => {
    if (!includeProjects.length) {
      alert('Please select at least one project or group')
      return
    }

    config.instance = instance
    config.pat = pat
    config.pollInterval = pollInterval
    config.includeGroups = includeGroups
    config.includeProjects = includeProjects

    back()
  }

  return (
    <Container>
      <Header>Settings</Header>

      <Section title='Configuration'>
        <Instance instance={instance} setInstance={setInstance} />
        <PersonalAccessToken pat={pat} setPAT={setPAT} />
        <PollInterval
          pollInterval={pollInterval}
          setPollInterval={setPollInterval}
        />
      </Section>

      <Section title='Groups &amp; Projects'>
        <GroupsAndProjects
          loading={loading}
          error={error}
          data={data}
          includeGroups={includeGroups}
          setIncludeGroups={setIncludeGroups}
          includeProjects={includeProjects}
          setIncludeProjects={setIncludeProjects}
        />
      </Section>

      <Section>
        <ButtonGroup onCancel={onCancel} onSave={onSave} />
      </Section>
    </Container>
  )
}

export default Settings
