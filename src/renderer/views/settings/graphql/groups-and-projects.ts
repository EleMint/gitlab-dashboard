import { ApolloError, gql, useQuery } from '@apollo/client'

import useConfig from '../../../providers/hooks/useConfig'

const QUERY = gql`
  query Settings {
    currentUser {
      groups {
        nodes {
          id
          fullPath
          name
          projects(includeSubgroups: true) {
            nodes {
              id
              fullPath
              name
            }
          }
        }
      }
    }
  }
`

export interface User {
  id: string
  groups: {
    nodes: Group[]
  }
}

export interface Group {
  id: string
  name: string
  fullPath: string
  projects: {
    nodes: Project[]
  }
}

export interface Project {
  id: string
  name: string
  fullPath: string
}

export interface Data {
  currentUser: User
}

interface UseGroupsAndProjects {
  loading: boolean
  error: ApolloError
  data: Data
}

const useGroupsAndProjects = (): UseGroupsAndProjects => {
  const { pollInterval } = useConfig()

  const { loading, error, data } = useQuery(QUERY, { pollInterval })

  return { loading, error, data }
}

export default useGroupsAndProjects
