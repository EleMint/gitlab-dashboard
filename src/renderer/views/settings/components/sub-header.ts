import styled from 'styled-components'

const SubHeader = styled.h4`
  color: ${props => props.theme.color.primary};
  margin: 0.75rem;
  margin-top: 2rem;
  font-size: 1.25rem;
  font-weight: 600;
  line-height: 1.25rem;
  user-select: none;
`

export default SubHeader
