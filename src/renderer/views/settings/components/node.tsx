import React from 'react'

import { Node } from '../../shared/tree'

import { Group, Project } from '../graphql/groups-and-projects'

import Name from './name'
import Path from './path'

const NodeComponent: React.FC<Node<Group, Project>> = ({ name, fullPath }) => (
  <>
    <Name>{name}</Name>
    <Path>{fullPath}</Path>
  </>
)

export default NodeComponent
