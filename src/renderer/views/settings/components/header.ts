import styled from 'styled-components'

const Header = styled.h1`
  margin: 1rem;
  color: ${props => props.theme.color.primary};
  font-size: 1.25rem;
  font-weight: 600;
  line-height: 1.25rem;
  user-select: none;
`

export default Header
