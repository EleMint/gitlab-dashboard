import React from 'react'

import Button from '../../shared/button'
import Column from './column'
import Row from './row'

interface ButtonGroupProps {
  onCancel: () => void
  onSave: () => void
}

const ButtonGroup: React.FC<ButtonGroupProps> = ({ onCancel, onSave }) => (
  <Row flex='right'>
    <Column>
      <Button marginRight={0.25} onClick={onCancel}>
        Cancel
      </Button>
    </Column>
    <Column>
      <Button primary onClick={onSave}>
        Save
      </Button>
    </Column>
  </Row>
)

export default ButtonGroup
