import styled from 'styled-components'

const Container = styled.div`
  margin: 0;
  width: 100%;
  height: 100vh;
  overflow-y: auto;
`

export default Container
