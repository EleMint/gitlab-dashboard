import styled from 'styled-components'

const LoadingWrapper = styled.div`
  color: ${props => props.theme.color.primary};
  margin: 0.5rem 1.5rem;
`

export default LoadingWrapper
