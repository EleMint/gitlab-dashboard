import styled from 'styled-components'

interface ColumnProps {
  flex?: 'left' | 'right'
  fullWidth?: boolean
  maxWidth?: number
  noHighlight?: boolean
}

const justifyContent = (value?: 'left' | 'right'): string => {
  if (value === undefined || value === 'left') return 'start'
  if (value === 'right') return 'end'
  return 'space-around'
}

const Column = styled.span<ColumnProps>`
  display: flex;
  align-self: center;
  justify-content: ${props => justifyContent(props.flex)};
  font-size: 1rem;
  ${props => (props.fullWidth ? 'width: 100%;' : '')}
  ${props => (props.maxWidth ? `max-width: ${props.maxWidth}rem;` : '')}
  ${props => (props.noHighlight ? 'user-select: none;' : '')}
`

export default Column
