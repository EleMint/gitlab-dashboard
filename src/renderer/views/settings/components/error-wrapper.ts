import styled from 'styled-components'

const ErrorWrapper = styled.div`
  color: ${props => props.theme.color.error};
  margin: 0.5rem 1.5rem;
`

export default ErrorWrapper
