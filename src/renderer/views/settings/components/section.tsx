import React from 'react'
import styled from 'styled-components'
import SubHeader from './sub-header'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`

interface SectionProps {
  title?: string
}

const Section: React.FC<SectionProps> = ({ title, children }) => (
  <Wrapper>
    {title && <SubHeader>{title}</SubHeader>}
    {children}
  </Wrapper>
)

export default Section
