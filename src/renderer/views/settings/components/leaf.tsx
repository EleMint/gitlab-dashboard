import React from 'react'

import { Leaf } from '../../shared/tree'

import { Project } from '../graphql/groups-and-projects'

import Name from './name'
import Path from './path'

const LeafComponent: React.FC<Leaf<Project>> = ({ name, fullPath }) => (
  <>
    <Name>{name}</Name>
    <Path>{fullPath}</Path>
  </>
)

export default LeafComponent
