import styled from 'styled-components'

const Name = styled.span`
  margin-left: 1rem;
`

export default Name
