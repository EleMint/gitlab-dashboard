import styled from 'styled-components'

const Path = styled.span`
  color: ${props => props.theme.color.secondary};
  margin-left: 0.2rem;
  font-style: italic;
`

export default Path
