import React from 'react'

import useConfig from '../../providers/hooks/useConfig'
import { ViewNavigation } from '../../providers/view'

import Loading from '../shared/loading'
import Error from '../shared/error'
import MR from '../shared/merge-request/mr'
import { MergeRequestState, MRStateRadios } from '../shared/merge-request/state'
import CopyText from '../shared/copy-text'
import RadioGroup from '../shared/radio-group'
import Toggle from '../shared/toggle'

import LoadingWrapper from './components/loading-wrapper'
import ErrorWrapper from './components/error-wrapper'
import Container from './components/container'
import List from './components/list'
import ListItem from './components/list-item'
import Column from './components/column'
import Header from './components/header'
import Path from './components/path'
import SubHeader from './components/subheader'
import Title from './components/title'
import Image from './components/image'
import Name from './components/name'

import useGroupQuery from './graphql/group-and-projects'

export interface GroupViewMetadata {
  id: string
  fullPath: string
  state?: MergeRequestState
}

type GroupProps = ViewNavigation & GroupViewMetadata

const Group: React.FC<GroupProps> = ({ fullPath, state }) => {
  const { pollInterval } = useConfig()

  const [mrState, setMRState] = React.useState<MergeRequestState>(
    state ?? MergeRequestState.OPENED
  )
  const [draft, setDraft] = React.useState(false)

  const mrStateLabel = React.useMemo(
    () => MRStateRadios.find(state => state.value === mrState).label,
    [mrState]
  )

  const { loading, error, data } = useGroupQuery(
    {
      fullPath,
      draft,
      state: mrState
    },
    pollInterval
  )

  if (loading && !data)
    return (
      <LoadingWrapper>
        <Loading />
      </LoadingWrapper>
    )

  if (error)
    return (
      <ErrorWrapper>
        <Error error={error} />
      </ErrorWrapper>
    )

  const { group } = data
  const {
    mergeRequests: { nodes: mrs }
  } = group

  return (
    <Container>
      <Title>
        {group.avatarUrl && (
          <Column direction='row'>
            <Column>
              <Image src={group.avatarUrl} />
            </Column>
          </Column>
        )}
        <Column>
          <Header>
            <Name onClick={() => window.open(group.webUrl, '_blank')}>
              {group.name}
            </Name>
            <Path>{group.fullPath}</Path>
          </Header>
          <SubHeader>
            Group ID: <CopyText value={group.id.split('/').pop()} />
          </SubHeader>
        </Column>
        <Column
          direction='column'
          align='end'
          justify='space-evenly'
          fullHeight
        >
          <Toggle enabled={draft} setEnabled={setDraft} label='Draft' />
          <RadioGroup
            radios={MRStateRadios}
            selected={mrState}
            setSelected={(selected: MergeRequestState) => setMRState(selected)}
          />
        </Column>
      </Title>
      <List>
        {!mrs.length && (
          <ListItem warning>No {mrStateLabel} Merge Requests</ListItem>
        )}

        {mrs.map(mr => (
          <ListItem key={mr.id}>
            <MR {...mr} />
          </ListItem>
        ))}
      </List>
    </Container>
  )
}

export default Group
