import styled from 'styled-components'

const SubHeader = styled.h6`
  color: ${props => props.theme.color.primary};
  font-size: 0.875rem;
  font-weight: 400;
  line-height: 1rem;
  margin-top: 0;
`

export default SubHeader
