import styled from 'styled-components'

interface ListItemProps {
  warning?: boolean
}

const ListItem = styled.li<ListItemProps>`
  display: flex;
  justify-content: space-between;
  margin-top: 1rem;
  color: ${props =>
    props.warning ? props.theme.color.warning : props.theme.color.primary};
`

export default ListItem
