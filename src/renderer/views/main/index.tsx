import React from 'react'

import useConfig from '../../providers/hooks/useConfig'
import { ViewNavigation } from '../../providers/view'
import MR from '../shared/merge-request/mr'

import { MergeRequestState, MRStateRadios } from '../shared/merge-request/state'
import RadioGroup from '../shared/radio-group'
import Toggle from '../shared/toggle'
import Column from './components/column'
import Container from './components/container'
import List from './components/list'
import ListItem from './components/list-item'
import Title from './components/title'

import useMergeRequests from './graphql/merge-requests'

export interface MainViewMetadata {
  state?: MergeRequestState
}

type MainProps = ViewNavigation & MainViewMetadata

const Main: React.FC<MainProps> = ({ state }) => {
  const { pollInterval } = useConfig()

  const [mrState, setMRState] = React.useState<MergeRequestState>(
    state ?? MergeRequestState.OPENED
  )
  const [draft, setDraft] = React.useState(false)

  const mrStateLabel = React.useMemo(
    () => MRStateRadios.find(state => state.value === mrState).label,
    [mrState]
  )

  const mrs = useMergeRequests(
    {
      draft,
      state: mrState
    },
    pollInterval
  )

  return (
    <Container>
      <Title>
        <Column
          direction='row'
          align='center'
          justify='space-around'
          fullHeight
          fullWidth
        >
          <RadioGroup
            radios={MRStateRadios}
            selected={mrState}
            setSelected={(selected: MergeRequestState) => setMRState(selected)}
          />
          <Toggle enabled={draft} setEnabled={setDraft} label='Draft' />
        </Column>
      </Title>
      <List>
        {!mrs.length && (
          <ListItem warning>No {mrStateLabel} Merge Requests</ListItem>
        )}

        {mrs.map(mr => (
          <ListItem key={mr.id}>
            <MR {...mr} />
          </ListItem>
        ))}
      </List>
    </Container>
  )
}

export default Main
