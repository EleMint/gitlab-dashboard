import React from 'react'
import { gql, useQuery } from '@apollo/client'

import useConfig from '../../../providers/hooks/useConfig'
import {
  IncludeGroupsType,
  IncludeProjectsType
} from '../../../providers/config/values'

import { MergeRequestState } from '../../shared/merge-request/state'

interface QUERY_VARIABLES {
  draft: boolean
  state: MergeRequestState
}

const QUERY = gql`
  query GroupsMergeRequest($draft: Boolean, $state: MergeRequestState) {
    currentUser {
      groups {
        nodes {
          id
          fullPath
          avatarUrl
          name
          webUrl
          mergeRequests(draft: $draft, state: $state) {
            nodes {
              id
              projectId
              title
              state
              createdAt
              updatedAt
              reference(full: true)
              webUrl
              author {
                webUrl
                username
              }
            }
          }
        }
      }
    }
  }
`

export interface User {
  groups: {
    nodes: Group[]
  }
}

export interface Group {
  id: string
  fullPath: string
  avatarUrl?: string
  name: string
  webUrl: string
  mergeRequests: {
    nodes: MergeRequest[]
  }
}

export interface MergeRequest {
  id: string
  projectId: number
  title: string
  state: MergeRequestState
  createdAt: string
  updatedAt: string
  reference: string
  webUrl: string
  author: Author
}

export interface Author {
  username: string
  webUrl: string
}

export interface Data {
  currentUser: User
}

const filterGroups = (
  groups: Group[],
  includeGroups: IncludeGroupsType,
  includeProjects: IncludeProjectsType
): MergeRequest[] =>
  groups
    .filter(group => includeGroups.includes(group.id))
    .map<MergeRequest[]>(group =>
      group.mergeRequests.nodes.filter(mr =>
        includeProjects.includes(`gid://gitlab/Project/${mr.projectId}`)
      )
    )
    .reduce<MergeRequest[]>((prev, curr) => prev.concat(curr), [])

const useGroups = (
  variables: QUERY_VARIABLES,
  pollInterval: number
): MergeRequest[] => {
  const { includeGroups, includeProjects } = useConfig()

  const [mrs, setMRS] = React.useState<MergeRequest[]>([])

  const { loading, error, data, refetch } = useQuery<Data>(QUERY, {
    variables,
    pollInterval
  })

  React.useEffect(() => {
    refetch(variables)
  }, [JSON.stringify(includeGroups), JSON.stringify(includeProjects)])

  React.useEffect(() => {
    if (!loading && error) {
      setMRS([])
      alert(error)
      return
    }

    if (!loading && data) {
      const _mrs = filterGroups(
        data.currentUser.groups.nodes,
        includeGroups,
        includeProjects
      )
      setMRS(_mrs)
      return
    }
  }, [loading])

  return mrs
}

export default useGroups
