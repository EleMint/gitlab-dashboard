import styled from 'styled-components'

interface ColumnProps {
  direction?: 'column' | 'row'
  align?: 'center' | 'start' | 'end'
  justify?: 'center' | 'space-evenly' | 'space-around'
  fullHeight?: boolean
  fullWidth?: boolean
}

const Column = styled.div<ColumnProps>`
  display: flex;
  flex-direction: ${props => props.direction ?? 'column'};
  justify-content: ${props => props.justify ?? 'center'};
  align-items: ${props => props.align ?? 'start'};
  ${props => (props.fullHeight ? 'height: 100%;' : '')}
  ${props => (props.fullWidth ? 'width: 100%;' : '')}
`

export default Column
