import styled from 'styled-components'

const Title = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 1rem;
`

export default Title
