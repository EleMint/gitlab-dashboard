import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0rem 1rem;
  width: 100%;
  height: 100vh;
  overflow-y: auto;
  color: ${props => props.theme.color.primary};
`

export default Container
