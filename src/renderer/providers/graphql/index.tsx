import React from 'react'
import { ApolloProvider } from '@apollo/client'

import useConfig from '../hooks/useConfig'

import clientFactory from './client'

const GraphQLProvider: React.FC = ({ children }) => {
  const { instance, pat } = useConfig()

  const client = React.useMemo(() => clientFactory(instance, pat), [
    instance,
    pat
  ])

  return <ApolloProvider client={client}>{children}</ApolloProvider>
}

export default GraphQLProvider
