import { ApolloClient, from, HttpLink, InMemoryCache } from '@apollo/client'

const client = (instance: string, pat: string) => {
  const uri = `${instance}/api/graphql`
  const Authorization = `Bearer ${pat}`

  return new ApolloClient({
    cache: new InMemoryCache(),
    uri,
    link: from([
      new HttpLink({
        uri,
        headers: {
          Authorization
        }
      })
    ])
  })
}

export default client
