import Configuration from '../config/Configuration'
import { View, ViewMetadata } from '../../views'

const VIEW_LS_KEY = 'view'
const METADATA_LS_KEY = 'metadata'

const defaultView = (config: Configuration): View => {
  const hasPAT = !!config.pat
  if (!hasPAT) return View.SETTINGS

  const hasProjects = !!config.includeProjects.length
  if (!hasProjects) return View.SETTINGS

  const ls = viewFromLS()
  if (ls) return ls

  return View.MAIN
}

const viewFromLS = (): View => {
  const view = localStorage.getItem(VIEW_LS_KEY) ?? ''

  const parsed = parseInt(view)

  if (Number.isNaN(parsed)) return View.MAIN

  return parsed
}

const metadataFromLS = (): ViewMetadata | undefined => {
  const metadata = localStorage.getItem(METADATA_LS_KEY) ?? ''

  if (!metadata) return

  const parsed = JSON.parse(metadata)

  return parsed
}

const viewToLS = (view?: View, metadata?: ViewMetadata) => {
  localStorage.setItem(VIEW_LS_KEY, `${view ?? View.MAIN}`)

  if (metadata) {
    localStorage.setItem(METADATA_LS_KEY, JSON.stringify(metadata))
  } else {
    localStorage.removeItem(METADATA_LS_KEY)
  }
}

const helpers = {
  defaultView,
  metadataFromLS,
  viewFromLS,
  viewToLS
}

export default helpers
