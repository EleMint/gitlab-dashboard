import React from 'react'

import useConfig from '../hooks/useConfig'

import { View, ViewMetadata } from '../../views'
import helpers from './helpers'

interface ViewContext {
  navigation: ViewNavigation
  previous?: View
  previousMetadata?: ViewMetadata
  current: View
  currentMetadata?: ViewMetadata
  navigate: (view: View, metadata?: ViewMetadata) => void
  back: () => void
  sidebar: boolean
  setSidebar: (sidebar: boolean) => void
  toggleSidebar: () => void
}

export interface ViewNavigation {
  current: View
  currentMetadata?: ViewMetadata
  previous?: View
  previousMetadata?: ViewMetadata
  navigate: (view: View, metadata?: ViewMetadata) => void
  back: () => void
  sidebarWidth: number
}

const initialView = {} as ViewContext

export const ViewContext = React.createContext<ViewContext>(initialView)

const ViewProvider: React.FC = ({ children }) => {
  const config = useConfig()
  const SIDEBAR_WIDTH = 30

  const [previous, setPrevious] = React.useState<View | undefined>()
  const [previousMetadata, setPreviousMetadata] = React.useState<
    ViewMetadata | undefined
  >(undefined)
  const [current, setCurrent] = React.useState<View>(
    helpers.defaultView(config)
  )
  const [currentMetadata, setCurrentMetadata] = React.useState<
    ViewMetadata | undefined
  >(helpers.metadataFromLS())

  const navigate = (view: View, metadata?: ViewMetadata) => {
    if (view === undefined) view = 0

    helpers.viewToLS(view, metadata)

    setPrevious(current)
    setPreviousMetadata(currentMetadata)
    setCurrent(view)
    setCurrentMetadata(metadata)
  }

  const back = () => {
    navigate(previous, previousMetadata)
  }

  const [sidebar, setSidebar] = React.useState(true)

  const toggleSidebar = () => {
    setSidebar(!sidebar)
  }

  const navigation: ViewNavigation = {
    current,
    currentMetadata,
    previous,
    previousMetadata,
    navigate,
    back,
    sidebarWidth: SIDEBAR_WIDTH
  }

  const value: ViewContext = {
    navigation,
    previous,
    previousMetadata,
    current,
    currentMetadata,
    navigate,
    back,
    sidebar,
    setSidebar,
    toggleSidebar
  }

  return <ViewContext.Provider value={value}>{children}</ViewContext.Provider>
}

export default ViewProvider
