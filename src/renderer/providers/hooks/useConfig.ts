import React from 'react'

import { ConfigContext } from '../config'

const useConfig = () => React.useContext(ConfigContext)

export default useConfig
