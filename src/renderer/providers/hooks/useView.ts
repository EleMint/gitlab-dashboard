import React from 'react'

import { ViewContext } from '../view'

const useView = () => React.useContext(ViewContext)

export default useView
