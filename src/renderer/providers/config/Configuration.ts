import {
  HotkeysType,
  IncludeGroupsType,
  IncludeProjectsType,
  InstanceType,
  IsDevelopmentType,
  PATType,
  PollIntervalType,
  ThemeType
} from './values'

type Dispatch<T> = React.Dispatch<React.SetStateAction<T>>

interface ConfigurationProps {
  _isDevelopment: IsDevelopmentType
  _setIsDevelopment: Dispatch<IsDevelopmentType>
  _instance: InstanceType
  _setInstance: Dispatch<InstanceType>
  _pat: PATType
  _setPAT: Dispatch<PATType>
  _pollInterval: PollIntervalType
  _setPollInterval: Dispatch<PollIntervalType>
  _theme: ThemeType
  _setTheme: Dispatch<ThemeType>
  _includeGroups: IncludeGroupsType
  _setIncludeGroups: Dispatch<IncludeGroupsType>
  _includeProjects: IncludeProjectsType
  _setIncludeProjects: Dispatch<IncludeProjectsType>
  _hotkeys: HotkeysType
  _setHotkeys: Dispatch<HotkeysType>
}

class Configuration {
  private _isDevelopment: IsDevelopmentType
  private _setIsDevelopment: Dispatch<IsDevelopmentType>

  public get isDevelopment(): IsDevelopmentType {
    return this._isDevelopment
  }

  public set isDevelopment(isDevelopment: IsDevelopmentType) {
    this._setIsDevelopment(isDevelopment)
  }

  private _instance: InstanceType
  private _setInstance: Dispatch<InstanceType>

  /**
   * GitLab Instance
   *
   * Defaults to https://gitlab.com
   *
   * Set using env: GITLAB_INSTANCE or in Settings
   */
  public get instance (): InstanceType {
    return this._instance
  }

  public set instance (instance: InstanceType) {
    localStorage.setItem('instance', instance)
    this._setInstance(instance)
  }

  private _pat: PATType
  private _setPAT: Dispatch<PATType>

  /**
   * GitLab Personal Access Token
   *
   * Set using env: GITLAB_PAT or in Settings
   *
   * If this value is empty, user is redirected to Settings
   */
  public get pat (): PATType {
    return this._pat
  }

  public set pat (pat: PATType) {
    localStorage.setItem('pat', pat)
    this._setPAT(pat)
  }

  private _pollInterval: PollIntervalType
  private _setPollInterval: Dispatch<PollIntervalType>

  /**
   * Poll Interval (in ms)
   *
   * Defaults to 60,000 (1 min)
   * Set to 0 to disable
   *
   * Determines time between data refresh
   */
  public get pollInterval (): PollIntervalType {
    return this._pollInterval
  }

  public set pollInterval (pollInterval: PollIntervalType) {
    localStorage.setItem('pollInterval', `${pollInterval}`)
    this._setPollInterval(pollInterval)
  }

  private _theme: ThemeType
  private _setTheme: Dispatch<ThemeType>

  public get theme (): ThemeType {
    return this._theme
  }

  public set theme (theme: ThemeType) {
    localStorage.setItem('theme', theme)
    this._setTheme(theme)
  }

  private _includeGroups: IncludeGroupsType
  private _setIncludeGroups: Dispatch<IncludeGroupsType>

  public get includeGroups (): IncludeGroupsType {
    return this._includeGroups
  }

  public set includeGroups (includeGroups: IncludeGroupsType) {
    localStorage.setItem('includeGroups', includeGroups.join('|'))
    this._setIncludeGroups(includeGroups)
  }

  private _includeProjects: IncludeProjectsType
  private _setIncludeProjects: Dispatch<IncludeProjectsType>

  public get includeProjects (): IncludeProjectsType {
    return this._includeProjects
  }

  public set includeProjects (includeProjects: IncludeProjectsType) {
    localStorage.setItem('includeProjects', includeProjects.join('|'))
    this._setIncludeProjects(includeProjects)
  }

  private _hotkeys: HotkeysType
  private _setHotkeys: Dispatch<HotkeysType>

  public get hotkeys (): HotkeysType {
    return this._hotkeys
  }

  public set hotkeys (hotkeys: HotkeysType) {
    localStorage.setItem('hotkeys', JSON.stringify(hotkeys))
    this._setHotkeys(hotkeys)
  }

  constructor ({
    _instance,
    _setInstance,
    _pat,
    _setPAT,
    _pollInterval,
    _setPollInterval,
    _theme,
    _setTheme,
    _includeGroups,
    _setIncludeGroups,
    _includeProjects,
    _setIncludeProjects,
    _hotkeys,
    _setHotkeys
  }: ConfigurationProps) {
    this._instance = _instance
    this._setInstance = _setInstance

    this._pat = _pat
    this._setPAT = _setPAT

    this._pollInterval = _pollInterval
    this._setPollInterval = _setPollInterval

    this._theme = _theme
    this._setTheme = _setTheme

    this._includeGroups = _includeGroups
    this._setIncludeGroups = _setIncludeGroups

    this._includeProjects = _includeProjects
    this._setIncludeProjects = _setIncludeProjects

    this._hotkeys = _hotkeys
    this._setHotkeys = _setHotkeys
  }
}

export default Configuration
