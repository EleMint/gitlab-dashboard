import React from 'react'

import Configuration from './Configuration'

import isDevelopment, { IsDevelopmentType } from './values/isDevelopment'
import instance, { InstanceType } from './values/instance'
import pat, { PATType } from './values/pat'
import pollInterval, { PollIntervalType } from './values/poll-interval'
import theme, { ThemeType } from './values/theme'
import includeGroups, { IncludeGroupsType } from './values/includeGroups'
import includeProjects, { IncludeProjectsType } from './values/includeProjects'
import hotkeys, { HotkeysType } from './values/hotkeys'

const initialConfig = {} as Configuration

export const ConfigContext = React.createContext<Configuration>(initialConfig)

const ConfigProvider: React.FC = ({ children }) => {
  const [_isDevelopment, _setIsDevelopment] = React.useState<IsDevelopmentType>(
    isDevelopment()
  )
  const [_instance, _setInstance] = React.useState<InstanceType>(instance())
  const [_pat, _setPAT] = React.useState<PATType>(pat())
  const [_pollInterval, _setPollInterval] = React.useState<PollIntervalType>(
    pollInterval()
  )
  const [_theme, _setTheme] = React.useState<ThemeType>(theme())
  const [_includeGroups, _setIncludeGroups] = React.useState<IncludeGroupsType>(
    includeGroups()
  )
  const [_includeProjects, _setIncludeProjects] = React.useState<
    IncludeProjectsType
  >(includeProjects())
  const [_hotkeys, _setHotkeys] = React.useState<HotkeysType>(hotkeys())

  const value = new Configuration({
    _isDevelopment,
    _setIsDevelopment,
    _instance,
    _setInstance,
    _pat,
    _setPAT,
    _pollInterval,
    _setPollInterval,
    _theme,
    _setTheme,
    _includeGroups,
    _setIncludeGroups,
    _includeProjects,
    _setIncludeProjects,
    _hotkeys,
    _setHotkeys
  })

  return (
    <ConfigContext.Provider value={value}>{children}</ConfigContext.Provider>
  )
}

export default ConfigProvider
