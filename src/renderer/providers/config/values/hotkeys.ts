export interface Hotkey {
  key: KeyboardEvent['key']
  meta?: {
    altKey?: KeyboardEvent['altKey']
    ctrlKey?: KeyboardEvent['ctrlKey']
  }
}

export interface HotkeysType {
  DATA: {
    RELOAD: Hotkey
  }
  VIEW: {
    TOGGLE_SIDEBAR: Hotkey
  }
}

const defaultHotkeys: HotkeysType = {
  DATA: {
    RELOAD: {
      key: 'r',
      meta: {
        ctrlKey: true
      }
    }
  },
  VIEW: {
    TOGGLE_SIDEBAR: {
      key: 's',
      meta: {
        ctrlKey: true
      }
    }
  }
}

const hotkeys = (): HotkeysType => {
  const ls = localStorage.getItem('hotkeys') ?? ''

  let map

  if (!ls) {
    map = defaultHotkeys
    localStorage.setItem('hotkeys', JSON.stringify(defaultHotkeys))
  } else {
    map = JSON.parse(ls)
  }

  return map as HotkeysType
}

export default hotkeys
