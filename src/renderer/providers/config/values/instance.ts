import { Window } from '.'

export type InstanceType = string

const instance = (): InstanceType => {
  const ls = localStorage.getItem('instance')

  const win = (window as unknown) as Window
  const env = win.electron.GITLAB_INSTANCE

  const def = 'https://gitlab.com'

  const value = ls || env || def

  if (!ls) localStorage.setItem('instance', value)

  return value
}

export default instance
