import { Window } from '.'

export type IsDevelopmentType = boolean

const isDevelopment = (): IsDevelopmentType => {
  const win = window as unknown as Window

  const env = win.electron.GITLAB_DASHBOARD_IS_DEVELOPMENT

  const bool = env === 'true'

  return bool
}

export default isDevelopment
