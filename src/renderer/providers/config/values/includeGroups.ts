export type IncludeGroupsType = string[]

const includeGroups = (): IncludeGroupsType => {
  const ls = localStorage.getItem('includeGroups')

  if (!ls) {
    localStorage.setItem('includeGroups', '')
    return []
  }

  const parsed = ls.split('|')

  return parsed
}

export default includeGroups
