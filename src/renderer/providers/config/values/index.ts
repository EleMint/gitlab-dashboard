export { default as isDevelopment, IsDevelopmentType } from './isDevelopment'
export { default as instance, InstanceType } from './instance'
export { default as pat, PATType } from './pat'
export { default as pollInterval, PollIntervalType } from './poll-interval'
export { default as theme, ThemeType } from './theme'
export { default as includeGroups, IncludeGroupsType } from './includeGroups'
export {
  default as includeProjects,
  IncludeProjectsType
} from './includeProjects'
export { default as hotkeys, HotkeysType } from './hotkeys'

export interface Window {
  electron: {
    GITLAB_INSTANCE: string
    GITLAB_PAT: string
    GITLAB_DASHBOARD_IS_DEVELOPMENT: 'true' | 'false'
  }
}
