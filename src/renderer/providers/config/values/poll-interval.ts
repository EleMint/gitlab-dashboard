export type PollIntervalType = number

const pollInterval = (): PollIntervalType => {
  const ls = localStorage.getItem('pollInterval')
  const parsed = parseInt(ls)

  const def = 60000

  const value = parsed || def

  if (!ls) localStorage.setItem('pollInterval', `${value}`)

  return value
}

export default pollInterval
