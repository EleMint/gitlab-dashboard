export type IncludeProjectsType = string[]

const includeProjects = (): IncludeProjectsType => {
  const ls = localStorage.getItem('includeProjects')

  if (!ls) {
    localStorage.setItem('includeProjects', '')
    return []
  }

  const parsed = ls.split('|')

  return parsed
}

export default includeProjects
