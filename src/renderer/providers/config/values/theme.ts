import themes, { Theme } from '../../../themes'

export type ThemeType = keyof typeof themes

const theme = (): ThemeType => {
  const ls = localStorage.getItem('theme')
  const parsed = Theme[ls as 'dark']

  const def = Theme.dark

  const value = parsed || def

  if (!ls) localStorage.setItem('theme', value)

  return value
}

export default theme
