import { Window } from '.'

export type PATType = string

const pat = (): PATType => {
  const ls = localStorage.getItem('pat')

  const win = (window as unknown) as Window
  const env = win.electron.GITLAB_PAT

  const def = ''

  const value = ls || env || def

  if (!ls) localStorage.setItem('pat', value)

  return value
}

export default pat
