import React from 'react'
import { ThemeProvider as Provider } from 'styled-components'

import useConfig from '../hooks/useConfig'

import themes from '../../themes'

const ThemeProvider: React.FC = ({ children }) => {
  const { theme } = useConfig()

  const themeObj = React.useMemo(() => themes[theme], [theme])

  return <Provider theme={themeObj}>{children}</Provider>
}

export default ThemeProvider
