import * as React from 'react'

import ConfigProvider from './providers/config'
import GraphQLProvider from './providers/graphql'
import ThemeProvider from './providers/theme'
import ViewProvider from './providers/view'

import Base from './views'

const App = () => (
  <ConfigProvider>
    <ThemeProvider>
      <GraphQLProvider>
        <ViewProvider>
          <Base />
        </ViewProvider>
      </GraphQLProvider>
    </ThemeProvider>
  </ConfigProvider>
)

export default <App />
